RustN64
=======
[![build status](https://gitlab.com/arthurc/rustn64/badges/master/build.svg)](https://gitlab.com/arthurc/rustn64/commits/master)

Blog Series
-----------
https://arthurcarlsson.se/categories/#rustn64

Nice-to-have Links
------------------
* [Sublime Text](https://www.sublimetext.com)
* [Hex Viewer Package for Sublime Text](https://github.com/facelessuser/HexViewer)
* [Online Disassembler](https://www.onlinedisassembler.com/odaweb/)
* [VR4300 Datasheet](http://datasheets.chipdb.org/NEC/Vr-Series/Vr43xx/U10504EJ7V0UMJ1.pdf)
* [ROM endianes info](http://jul.rustedlogic.net/thread.php?id=11769)
