use byteorder::BigEndian;

use cpu::mem;

pub enum PifRom {}
impl PifRom {
  pub fn new(rom: Vec<u8>) -> mem::Mem<PifRom> {
    mem::Mem::new(rom)
  }
}

impl mem::MemCtx for PifRom {
  type ByteOrder = BigEndian;
}
