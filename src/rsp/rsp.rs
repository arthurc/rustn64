use byteorder::BigEndian;
use cpu::mem;

pub enum Rsp {}

impl Rsp {
  pub fn new() -> mem::Mem<Rsp> {
    mem::Mem::with_capacity(0x000F_FFFF)
  }
}

impl mem::MemCtx for Rsp {
  type ByteOrder = BigEndian;
}
