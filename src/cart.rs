use byteorder::BigEndian;
use rom::Rom;

pub struct Cart {
  rom: Rom<BigEndian>,
}

impl Cart {
  pub fn new(buf: Vec<u8>) -> Cart {
    let mut rom = Rom::new(buf);

    match rom[0] {
      0x37 => rom.byte_swap(),
      0x40 => rom.endian_swap(),
      _ => (),
    }

    Cart { rom: rom }
  }

  pub fn title(&self) -> String {
    self.rom.read_ascii_string(0x0020, 20)
  }
}

impl Cart {
  pub fn read_word(&self, addr: u32) -> u32 {
    self.rom.read_word(addr)
  }
}
