pub struct Regs {
  /// 64-bit Program Counter, the PC register
  pub pc: u64,
  /// The general purpose registers
  gpr: [u64; 32],
  /// 32 64-bit floating-point operation registers, FPRs
  pub fpr: [u64; 32],
  /// 64-bit HI register, containing the integer multiply and divide high- order doubleword result
  pub hi_res: u64,
  /// 64-bit LO register, containing the integer multiply and divide low- order doubleword result
  pub lo_res: u64,
  /// 1-bit Load/Link LLBit register
  pub llbit: bool,
  /// 32-bit floating-point Implementation/Revision register, FCR0
  pub fcr0: u32,
  /// 32-bit floating-point Control/Status register, FCR31
  pub fcr31: u32,
}

impl Regs {
  pub fn new() -> Regs {
    Default::default()
  }

  #[inline]
  pub fn gpr(&self, nr: impl Into<u32>) -> u64 {
    self.gpr[nr.into() as usize]
  }

  #[inline]
  pub fn gpr_mut(&mut self, nr: impl Into<u32>) -> &mut u64 {
    &mut self.gpr[nr.into() as usize]
  }
}

impl Default for Regs {
  /// Creates a new Regs instance and sets the register values to
  /// bogus values so that they can be determined if they have been changed or not
  fn default() -> Self {
    Self {
      pc: 0xcafebabe,
      gpr: [0xcafebabe; 32],
      fpr: [0xcafebabe; 32],
      hi_res: 0xcafebabe,
      lo_res: 0xcafebabe,
      llbit: true,
      fcr0: 0xcafebabe,
      fcr31: 0xcafebabe,
    }
  }
}
