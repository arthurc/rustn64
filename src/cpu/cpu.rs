use cpu::{self, mem, reg};

/// The main type for emulating the CPU, which is a VR4300 MIPS processor.
pub struct Cpu {
  regs: reg::Regs,
  cp0: cpu::Cp0,
}

impl Cpu {
  /// Creates a blank CPU instance which is in an undefined state.
  pub fn new() -> Self {
    Self {
      regs: reg::Regs::new(),
      cp0: cpu::Cp0::new(),
    }
  }

  /// Triggers a power-on reset exception as per the section 9.2.1 in the datasheet
  /// TODO: Maybe handle this as an exception which sets an exception bit?
  pub fn power_on_reset(&mut self) {
    self.regs.pc = 0xFFFF_FFFF_BFC0_0000;
    self.cp0.power_on_reset();
  }

  /// Fetches the instruction for the current PC
  pub fn fetch(&self, mem: &impl mem::Read) -> u32 {
    mem.readw(self.cp0.virt_addr_to_phys_addr(self.regs.pc))
  }

  /// Executes the instruction in the context of the CPU.
  ///
  /// NOTE: Only handles 32 bit mode.
  pub fn execute(&mut self, instr: &cpu::Instruction, mem: &mut (impl mem::Read + mem::Write)) {
    use cpu::Instruction::*;

    println!("{:?}", instr);

    match *instr {
      ADDIU(ref a) => *self.regs.gpr_mut(a.rt()) = self.regs.gpr(a.rs()) + a.imm_sign_extended(),
      ANDI(ref a) => *self.regs.gpr_mut(a.rt()) = (a.imm() as u64) & self.regs.gpr(a.rs()),
      BNEL(ref a) => {
        let target = a.imm_sign_extended() << 2;
        let condition = self.regs.gpr(a.rs()) != self.regs.gpr(a.rt());

        if condition {
          self.regs.pc = self.regs.pc.wrapping_add(target - 4);
        }
      }
      BEQL(ref a) => {
        let target = a.imm_sign_extended() << 2;
        let condition = self.regs.gpr(a.rs()) == self.regs.gpr(a.rt());

        if condition {
          self.regs.pc = self.regs.pc.wrapping_add(target - 4);
        }
      }
      LUI(ref a) => *self.regs.gpr_mut(a.rt()) = a.imm_sign_extended() << 16,
      LW(ref a) => {
        let base = a.rs();
        let offset = a.imm_sign_extended();

        let vaddr = self.regs.gpr(base).wrapping_add(offset);
        let paddr = self.cp0.virt_addr_to_phys_addr(vaddr);
        let mem = mem.readw(paddr) as u64;
        *self.regs.gpr_mut(a.rt()) = mem;
      }
      MTC0(ref a) => *self.cp0.regs.gpr_mut(a.rd()) = self.regs.gpr(a.rt()) as u32,
      ORI(ref a) => *self.regs.gpr_mut(a.rt()) = self.regs.gpr(a.rs()) | (a.imm() as u64),
      SW(ref a) => {
        let vaddr = a.imm_sign_extended() + self.regs.gpr(a.rs());
        let paddr = self.cp0.virt_addr_to_phys_addr(vaddr);
        let data = self.regs.gpr(a.rt()) as u32;

        mem.writew(paddr, data);
      }
      REGIMM(..) => unimplemented!("REGIMM"),
      ADD(..) => unimplemented!("ADD"),
      SRL(..) => unimplemented!("SRL"),
    }

    self.regs.pc += 4;
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use byteorder::BigEndian;

  enum TestMemCtx {}
  impl mem::MemCtx for TestMemCtx {
    type ByteOrder = BigEndian;
  }

  mod execute {
    use super::*;
    use cpu::instruction::{IType, RType};
    use cpu::Instruction::*;

    #[test]
    fn test_mtc0() {
      let mut cpu = Cpu::new();
      *cpu.regs.gpr_mut(1u32) = 66u64;
      *cpu.cp0.regs.gpr_mut(8u32) = 10u32;

      cpu.execute(
        &MTC0(RType::new(99, 1, 8, 99, 99)),
        &mut mem::Mem::<TestMemCtx>::with_capacity(1000),
      );

      assert_eq!(66, cpu.cp0.regs.gpr(8u32));
    }

    #[test]
    fn test_lui() {
      let mut cpu = Cpu::new();

      cpu.execute(
        &LUI(IType::new(1, 2, 66)),
        &mut mem::Mem::<TestMemCtx>::with_capacity(1000),
      );

      assert_eq!(66 << 16, cpu.regs.gpr(2u32));
    }

    #[test]
    fn test_ori() {
      let mut cpu = Cpu::new();
      *cpu.regs.gpr_mut(1u32) = 0xABCD_0000u64 | 0b1001;

      cpu.execute(
        &ORI(IType::new(1, 2, 0b1110)),
        &mut mem::Mem::<TestMemCtx>::with_capacity(1000),
      );

      assert_eq!(0xABCD_0000 | 0b1111, cpu.regs.gpr(2u32));
    }

    #[test]
    fn test_lw() {
      let mut cpu = Cpu::new();
      *cpu.regs.gpr_mut(1u32) = 0xFFFF_FFFF_A000_0004;

      cpu.execute(
        &LW(IType::new(1, 2, 3)),
        &mut mem::Mem::<TestMemCtx>::new(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 0xa, 0xb]),
      );

      assert_eq!(0x08090a0b, cpu.regs.gpr(2u32));
    }

    #[test]
    fn test_bneq_equals() {
      let mut cpu = Cpu::new();
      *cpu.regs.gpr_mut(1u32) = 10;
      *cpu.regs.gpr_mut(2u32) = 10;
      cpu.regs.pc = 99;

      cpu.execute(
        &BNEL(IType::new(1, 2, 10)),
        &mut mem::Mem::<TestMemCtx>::new(vec![]),
      );

      assert_eq!(103, cpu.regs.pc);
    }

    #[test]
    fn test_bneq_not_equals() {
      let mut cpu = Cpu::new();
      *cpu.regs.gpr_mut(1u32) = 10;
      *cpu.regs.gpr_mut(2u32) = 11;
      cpu.regs.pc = 99;

      cpu.execute(
        &BNEL(IType::new(1, 2, 0b01000)),
        &mut mem::Mem::<TestMemCtx>::new(vec![]),
      );

      assert_eq!(99 + 0b0100000, cpu.regs.pc);
    }
  }
}
