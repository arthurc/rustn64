use std::fmt;

/// An I-Type instruction format as per chapter 1.4.3 in the datasheet
/// 31    26 25    21 20    16 15     0
///       op       rs       rt        imm
#[derive(PartialEq)]
pub struct IType(u32);
impl IType {
  #[inline]
  pub fn new(rs: u8, rt: u8, imm: u16) -> Self {
    IType(((rs as u32) & 0b11111) << 21 | ((rt as u32) & 0b11111) << 16 | (imm as u32))
  }

  #[inline]
  pub fn rs(&self) -> u8 {
    ((self.0 >> 21) & 0b11111) as u8
  }

  #[inline]
  pub fn rt(&self) -> u8 {
    ((self.0 >> 16) & 0b11111) as u8
  }

  #[inline]
  pub fn imm(&self) -> u16 {
    (self.0 & 0xFFFF) as u16
  }

  #[inline]
  pub fn imm_sign_extended(&self) -> u64 {
    (self.imm() as i16) as u64
  }
}

impl fmt::Debug for IType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "rs: 0x{:X}, rt: 0x{:X}, imm: 0x{:X}",
      self.rs(),
      self.rt(),
      self.imm()
    )
  }
}

/// A J-Type instruction format as per chapter 1.4.3 in the datasheet
/// 31     26 25    0
///        op       target
#[derive(PartialEq)]
pub struct JType(u32);
impl JType {
  #[inline]
  pub fn target(&self) -> u32 {
    self.0 & 0x3FFF_FFFF
  }
}

impl fmt::Debug for JType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "target: 0x{:X}", self.target())
  }
}

/// A R-Type instruction format as per chapter 1.4.3 in the datasheet
/// 31    26 25    21 20    16 15    11 10     6 5    0
///       op       rs       rt       rd        sa     funct
#[derive(PartialEq)]
pub struct RType(u32);
impl RType {
  #[inline]
  pub fn new(rs: u8, rt: u8, rd: u8, sa: u8, funct: u8) -> Self {
    RType(
      ((rs as u32) & 0b11111) << 21
        | ((rt as u32) & 0b11111) << 16
        | ((rd as u32) & 0b11111) << 11
        | ((sa as u32) & 0b11111) << 6
        | (funct as u32),
    )
  }

  #[inline]
  pub fn rs(&self) -> u8 {
    ((self.0 >> 21) & 0b11111) as u8
  }

  #[inline]
  pub fn rt(&self) -> u8 {
    ((self.0 >> 16) & 0b11111) as u8
  }

  #[inline]
  pub fn rd(&self) -> u8 {
    ((self.0 >> 11) & 0b11111) as u8
  }

  #[inline]
  pub fn sa(&self) -> u8 {
    ((self.0 >> 6) & 0b11111) as u8
  }

  #[inline]
  pub fn funct(&self) -> u8 {
    (self.0 & 0b111111) as u8
  }
}

impl fmt::Debug for RType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "rs: 0x{:X}, rt: 0x{:X}, rd: 0x{:X}, sa: 0x{:X}, funct: 0x{:X}",
      self.rs(),
      self.rt(),
      self.rd(),
      self.sa(),
      self.funct()
    )
  }
}

/// Instruction representations. The instructions are described in in chapter 16 in the datasheet.
#[derive(Debug, PartialEq)]
pub enum Instruction {
  LUI(IType),
  ADDIU(IType),
  MTC0(RType),
  ORI(IType),
  LW(IType),
  ANDI(IType),
  BEQL(IType),
  SW(IType),
  BNEL(IType),
  REGIMM(IType),
  SRL(RType),
  ADD(RType),
}

/// Decodes an instruction to an Instruction type. Returns a DecodeError if
/// there was an error.
#[inline]
pub fn decode(word: u32) -> Instruction {
  use self::Instruction::*;

  match word >> 26 {
    // SPECIAL
    0b000000 => match word & 0b111111 {
      0b000010 => SRL(RType(word)),
      0b100000 => ADD(RType(word)),
      w @ _ => panic!("Unknown special instruction: {:X}", w),
    },
    0b000001 => REGIMM(IType(word)),
    0b001001 => ADDIU(IType(word)),
    0b001100 => ANDI(IType(word)),
    0b001101 => ORI(IType(word)),
    0b001111 => LUI(IType(word)),
    // COP0
    0b010000 => match (word >> 21) & 0b11111 {
      0b00100 => MTC0(RType(word)),
      w @ _ => panic!("Unknown CP0 instruction: {:X}", w),
    },
    0b010100 => BEQL(IType(word)),
    0b100011 => LW(IType(word)),
    0b101011 => SW(IType(word)),
    0b010101 => BNEL(IType(word)),
    w @ _ => panic!("Unknown instruction: {:01$b}", w, 6),
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  mod itype {
    use super::*;

    #[test]
    fn given_a_word_then_it_should_be_able_to_extract_the_itype_parts_of_the_instruction() {
      let subject = IType(0x1234_5678);

      assert_eq!(subject.rs(), 0b10001);
      assert_eq!(subject.rt(), 0b10100);
      assert_eq!(subject.imm(), 0b0101011001111000);
    }
  }

  mod rtype {
    use super::*;

    #[test]
    fn given_a_word_then_it_should_be_able_to_extract_the_rtype_parts_of_the_instruction() {
      let subject = RType(0x1234_5678);

      assert_eq!(subject.rs(), 0b10001);
      assert_eq!(subject.rt(), 0b10100);
      assert_eq!(subject.rd(), 0b01010);
      assert_eq!(subject.sa(), 0b11001);
      assert_eq!(subject.funct(), 0b111000);
    }
  }

  mod jtype {
    use super::*;

    #[test]
    fn given_a_word_then_it_should_be_able_to_extract_the_jtype_parts_of_the_instruction() {
      let subject = JType(0x1234_5678);

      assert_eq!(subject.target(), 0x1234_5678);
    }
  }

  mod decode {
    use super::*;

    #[test]
    fn given_the_regimm_word_then_it_should_resolve_to_regimm() {
      assert_eq!(Instruction::REGIMM(IType(0x0600_0000)), decode(0x0600_0000));
    }

    #[test]
    fn given_the_srl_word_then_it_should_resolve_to_srl() {
      assert_eq!(Instruction::SRL(RType(0x0000_0002)), decode(0x0000_0002));
    }

    #[test]
    fn given_an_instruction_word_then_it_should_be_decodable() {
      assert_eq!(
        Instruction::ADDIU(IType(0b001001_10101_00110_0000111100001111)),
        decode(0b001001_10101_00110_0000111100001111)
      );
    }

    #[test]
    fn given_a_special_instruction_word_then_it_should_be_decodable() {
      assert_eq!(
        Instruction::ADD(RType(0b000000_10101_00110_11001_00000_100000)),
        decode(0b000000_10101_00110_11001_00000_100000)
      );
    }

    #[test]
    fn given_a_cop0_instruction_word_then_it_should_be_decodable() {
      assert_eq!(Instruction::MTC0(RType(0x40800000)), decode(0x40800000));
    }
  }
}
