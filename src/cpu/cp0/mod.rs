pub use self::cp0::Cp0;
pub use self::reg::Regs;

mod cp0;
mod reg;
