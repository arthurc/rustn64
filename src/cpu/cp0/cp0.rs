use super::{reg, Regs};

enum Segment {
  // 32-bit Supervisor mode segments
  SUSEG,
  SSEG,

  // 32-bit Kernel mode segments
  KUSEG,
  KSEG0,
  KSEG1,
  KSSEG,
  KSEG3,

  // 64-bit Kernel mode segments
  XKUSEG,
  XKSSEG,
  XKPHYS,
  XKSEG,
  CKSEG0,
  CKSEG1,
  CKSSEG,
  CKSEG3,
}

#[derive(Default)]
pub struct Cp0 {
  pub regs: Regs,
}

impl Cp0 {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn power_on_reset(&mut self) {
    self
      .regs
      .remove_status_flags(reg::Status::DS_TS | reg::Status::DS_SR | reg::Status::RP)
      .remove_config_flags(reg::Config::EP)
      .add_status_flags(reg::Status::ERL | reg::Status::DS_BEV)
      .add_config_flags(reg::Config::BE);
  }

  /// Maps a virtual address to a physical address as per section 5.2 in the datasheet
  pub fn virt_addr_to_phys_addr(&self, vaddr: u64) -> u32 {
    use self::Segment::*;

    match self.segment(vaddr) {
      // 32-bit Supervisor mode segments
      SUSEG => unimplemented!("KSSEG"),
      SSEG => unimplemented!("KSSEG"),

      // 32-bit Kernel mode segments
      KUSEG => unimplemented!("KUSEG"),
      KSEG0 => unimplemented!("KSEG0"),
      KSEG1 => (vaddr as u32) - 0xA000_0000,
      KSSEG => unimplemented!("KSSEG"),
      KSEG3 => unimplemented!("KSEG3"),

      // 64-bit Kernel mode segments
      XKUSEG => unimplemented!("XKUSEG"),
      XKSSEG => unimplemented!("XKSSEG"),
      XKPHYS => unimplemented!("XKPHYS"),
      XKSEG => unimplemented!("XKSEG"),
      CKSEG0 => unimplemented!("CKSEG0"),
      CKSEG1 => (vaddr - 0xFFFF_FFFF_A000_0000) as u32,
      CKSSEG => unimplemented!("CKSSEG"),
      CKSEG3 => unimplemented!("CKSEG3"),
    }
  }

  fn segment(&self, vaddr: u64) -> Segment {
    use self::Segment::*;

    if self.regs.status().is_32_bit_kernel_mode() {
      return match vaddr as u32 {
        0x0000_0000...0x7FFF_FFFF => KUSEG,
        0x8000_0000...0x9FFF_FFFF => KSEG0,
        0xA000_0000...0xBFFF_FFFF => KSEG1,
        0xC000_0000...0xDFFF_FFFF => KSSEG,
        0xE000_0000...0xFFFF_FFFF => KSEG3,
        _ => panic!("is_32_bit_kernel_mode - Should not happen..."),
      };
    } else if self.regs.status().is_64_bit_kernel_mode() {
      return match vaddr {
        0x0000_0000_0000_0000...0x0000_00FF_FFFF_FFFF => XKUSEG,
        0x4000_0000_0000_0000...0x4000_00FF_FFFF_FFFF => XKSSEG,
        0x8000_0000_0000_0000...0xBFFF_FFFF_FFFF_FFFF => XKPHYS,
        0xC000_0000_0000_0000...0xC000_00FF_7FFF_FFFF => XKSEG,
        0xFFFF_FFFF_8000_0000...0xFFFF_FFFF_9FFF_FFFF => CKSEG0,
        0xFFFF_FFFF_A000_0000...0xFFFF_FFFF_BFFF_FFFF => CKSEG1,
        0xFFFF_FFFF_C000_0000...0xFFFF_FFFF_DFFF_FFFF => CKSSEG,
        0xFFFF_FFFF_E000_0000...0xFFFF_FFFF_FFFF_FFFF => CKSEG3,
        _ => panic!("is_64_bit_kernel_mode - Should not happen..."),
      };
    } else if self.regs.status().is_64_bit_user_mode() {
      unimplemented!("64 bit user mode")
    } else if self.regs.status().is_32_bit_user_mode() {
      unimplemented!("32 bit user mode")
    } else if self.regs.status().is_64_bit_supervisor_mode() {
      unimplemented!("64 bit supervisor mode")
    } else if self.regs.status().is_32_bit_supervisor_mode() {
      return match vaddr as u32 {
        0x0000_0000...0x7FFF_FFFF => SUSEG,
        0xC000_0000...0xDFFF_FFFF => SSEG,
        _ => panic!("is_32_bit_supervisor_mode - Should not happen..."),
      };
    } else {
      panic!("Unknown segment")
    }
  }
}
