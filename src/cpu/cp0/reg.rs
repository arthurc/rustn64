pub const STATUS: usize = 12;
pub const CONFIG: usize = 16;

/// CP0 system registers
pub struct Regs {
  gpr: [u32; 32],
}

impl Regs {
  pub fn gpr(&self, nr: impl Into<u32>) -> u32 {
    self.gpr[nr.into() as usize]
  }

  pub fn gpr_mut(&mut self, nr: impl Into<u32>) -> &mut u32 {
    &mut self.gpr[nr.into() as usize]
  }

  pub fn status(&self) -> Status {
    Status::from_bits_truncate(self.gpr[STATUS])
  }

  pub fn config(&self) -> Config {
    Config::from_bits_truncate(self.gpr[CONFIG])
  }

  pub fn remove_status_flags(&mut self, flags: Status) -> &mut Self {
    self.gpr[STATUS] = (self.status() - flags).bits;
    self
  }

  pub fn remove_config_flags(&mut self, flags: Config) -> &mut Self {
    self.gpr[CONFIG] = (self.config() - flags).bits;
    self
  }

  pub fn add_status_flags(&mut self, flags: Status) -> &mut Self {
    self.gpr[STATUS] = (self.status() | flags).bits;
    self
  }

  pub fn add_config_flags(&mut self, flags: Config) -> &mut Self {
    self.gpr[CONFIG] = (self.config() | flags).bits;
    self
  }
}

impl Default for Regs {
  fn default() -> Self {
    Self {
      gpr: [0xcafebabe; 32],
    }
  }
}

bitflags! {

  /// Status registers as per "Figure 6-5 Status Register" in the datasheet
  pub struct Status: u32 {
    const IE  = 1 << 0;
    const EXL = 1 << 1;
    const ERL = 1 << 2;
    const KSU = 0b11 << 3;
    const UX  = 1 << 5;
    const SX  = 1 << 6;
    const KX  = 1 << 7;
    const IM  = 0b1111_1111 << 8;
    const DS_DE = 1 << 15;
    const DS_CE = 1 << 16;
    const DS_CH = 1 << 17;
    // ... 0
    const DS_SR = 1 << 19;
    const DS_TS = 1 << 20;
    const DS_BEV = 1 << 21;
    // ... 0
    const DS_ITS = 1 << 23;
    const RE = 1 << 24;
    const FR = 1 << 25;
    const RP = 1 << 26;
    const CU = 0b1111 << 27;

    const DS = Self::DS_DE.bits
      | Self::DS_CE.bits
      | Self::DS_CH.bits
      | Self::DS_SR.bits
      | Self::DS_TS.bits
      | Self::DS_BEV.bits
      | Self::DS_ITS.bits;

    const KSU_USER = 0b10 << 3;
    const KSU_SUPERVISOR = 0b01 << 3;
    const KSU_KERNEL = 0;
  }

}

impl Status {
  /// Table 5-1 32-Bit User Mode Segments
  pub fn is_32_bit_user_mode(&self) -> bool {
    (*self & Status::KSU) == Status::KSU_USER
      && (*self & Status::EXL).bits == 0
      && (*self & Status::ERL).bits == 0
      && (*self & Status::UX).bits == 0
  }

  /// Table 5-1 64-Bit User Mode Segments
  pub fn is_64_bit_user_mode(&self) -> bool {
    (*self & Status::KSU) == Status::KSU_USER
      && (*self & Status::EXL).bits == 0
      && (*self & Status::ERL).bits == 0
      && (*self & Status::UX).bits != 0
  }

  /// Table 5-2 32-Bit Supervisor Mode Segments
  pub fn is_32_bit_supervisor_mode(&self) -> bool {
    (*self & Status::KSU) == Status::KSU_SUPERVISOR
      && (*self & Status::EXL).bits == 0
      && (*self & Status::ERL).bits == 0
      && (*self & Status::SX).bits == 0
  }

  /// Table 5-2 64-Bit Supervisor Mode Segments
  pub fn is_64_bit_supervisor_mode(&self) -> bool {
    (*self & Status::KSU) == Status::KSU_SUPERVISOR
      && (*self & Status::EXL).bits == 0
      && (*self & Status::ERL).bits == 0
      && (*self & Status::SX).bits != 0
  }

  /// Table 5-3 32-Bit Kernel Mode Segments
  pub fn is_32_bit_kernel_mode(&self) -> bool {
    ((*self & Status::KSU) == Status::KSU_KERNEL
      || (*self & Status::EXL).bits != 0
      || (*self & Status::ERL).bits != 0) && (*self & Status::KX).bits == 0
  }

  /// Table 5-4 64-Bit Kernel Mode Segments
  pub fn is_64_bit_kernel_mode(&self) -> bool {
    ((*self & Status::KSU) == Status::KSU_KERNEL
      || (*self & Status::EXL).bits != 0
      || (*self & Status::ERL).bits != 0) && (*self & Status::KX).bits != 0
  }
}

bitflags! {

  /// Config register as per "Figure 5-16 Config Register" in the datasheet
  pub struct Config: u32 {
    const K0 = 0b111 << 0;
    const CU = 1 << 3;
    // ... 1100100110
    const BE = 1 << 15;
    // ... 00000110
    const EP = 0b1111 << 24;
    const EC = 0b111 << 28;
    // ... 0
  }

}
