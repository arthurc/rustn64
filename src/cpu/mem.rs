use byteorder;
use byteorder::ByteOrder;
use std::marker::PhantomData;

pub trait MemCtx {
  type ByteOrder: byteorder::ByteOrder;
}

pub struct Mem<T: MemCtx> {
  buf: Vec<u8>,
  phantom: PhantomData<T>,
}

impl<T: MemCtx> Mem<T> {
  /// Creates a new memory buffer with the pre-filled memory content
  pub fn new(data: Vec<u8>) -> Self {
    Self {
      buf: data,
      phantom: PhantomData,
    }
  }

  /// Creates a memory buffer with the specified size
  pub fn with_capacity(size: usize) -> Self {
    Self {
      buf: vec![0xab; size],
      phantom: PhantomData,
    }
  }
}

/// Reader for the memory as seen from the CPU's perspective
pub trait Read {
  fn readb(&self, addr: u32) -> u8;

  /// Reads a word from the memory at the given address.
  fn readw(&self, addr: u32) -> u32;
}
impl<T: MemCtx> Read for Mem<T> {
  fn readb(&self, addr: u32) -> u8 {
    self.buf[addr as usize]
  }

  fn readw(&self, addr: u32) -> u32 {
    T::ByteOrder::read_u32(&self.buf[addr as usize..addr as usize + 4])
  }
}

/// Writer for the memory as seen from the CPU's perspective
pub trait Write {
  /// Writes a byte to the given address
  fn writeb(&mut self, addr: u32, byte: u8);

  /// Writes a word to the given address
  ///
  /// TODO: What happens if there is no room for the word? I.e. the address is just at the end of the memory.
  fn writew(&mut self, addr: u32, word: u32);
}

impl<T: MemCtx> Write for Mem<T> {
  fn writeb(&mut self, addr: u32, byte: u8) {
    self.buf[addr as usize] = byte
  }

  fn writew(&mut self, addr: u32, word: u32) {
    T::ByteOrder::write_u32(&mut self.buf[addr as usize..addr as usize + 4], word)
  }
}
