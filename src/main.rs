extern crate clap;
extern crate rustn64;

use clap::{App, Arg};
use std::fs::File;
use std::io::Read;
use std::path::Path;

use rustn64::{Cart, N64, PifRom};

fn main() {
  let matches = App::new("RustN64")
    .version(env!("CARGO_PKG_VERSION"))
    .author("Arthur Carlsson <arthur@kiron.net>")
    .about("A Nintendo 64 emulator written in Rust")
    .arg(
      Arg::with_name("PIF")
        .help("The PIF ROM file to use")
        .short("p")
        .long("pif")
        .use_delimiter(false)
        .takes_value(true)
        .required(true),
    )
    .arg(
      Arg::with_name("ROM")
        .help("The ROM file to use")
        .use_delimiter(false)
        .required(true)
        .index(1),
    )
    .get_matches();

  let pif = read_bin(matches.value_of("PIF").unwrap());
  let rom = read_bin(matches.value_of("ROM").unwrap());

  let pif_rom = PifRom::new(pif);
  let cart = Cart::new(rom);

  println!("Running {}", cart.title());

  let mut n64 = N64::new(pif_rom);
  n64.insert_cart(cart);
  n64.power_on();
  n64.start()
}

fn read_bin<P: AsRef<Path>>(path: P) -> Vec<u8> {
  let mut buf = vec![];

  match File::open(path.as_ref()).and_then(|mut f| f.read_to_end(&mut buf)) {
    Ok(_) => buf,
    Err(e) => {
      println!("Failed to load \"{}\": {}", path.as_ref().display(), e);
      std::process::exit(1);
    }
  }
}
