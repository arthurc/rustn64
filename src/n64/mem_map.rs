use cpu::mem;
use pif_rom::PifRom;
use rsp::Rsp;

pub struct MemoryMap {
  pub rsp: mem::Mem<Rsp>,
  pub pif_rom: mem::Mem<PifRom>,
}

impl MemoryMap {
  pub fn new(pif_rom: mem::Mem<PifRom>, rsp: mem::Mem<Rsp>) -> Self {
    Self { pif_rom, rsp }
  }

  fn read<'a, T>(&'a self, f: impl FnOnce(&'a mem::Read, u32) -> T, paddr: u32) -> T {
    let (reader, offset) = match paddr {
      0x0400_0000...0x040F_FFFF => (&self.rsp as &mem::Read, 0x0400_0000),
      0x1FC0_0000...0x1FC0_07BF => (&self.pif_rom as &mem::Read, 0x1FC0_0000),
      _ => panic!("Unknown physical address: 0x{:X}", paddr),
    };

    f(reader, paddr - offset)
  }

  fn write<'a, T>(&'a mut self, f: impl FnOnce(&'a mut mem::Write, u32, T), paddr: u32, data: T) {
    let (writer, offset) = match paddr {
      0x0400_0000...0x040F_FFFF => (&mut self.rsp as &mut mem::Write, 0x0400_0000),
      0x1FC0_0000...0x1FC0_07BF => (&mut self.pif_rom as &mut mem::Write, 0x1FC0_0000),
      _ => panic!("Unknown physical address: 0x{:X}", paddr),
    };

    f(writer, paddr - offset, data);
  }
}

impl mem::Read for MemoryMap {
  fn readw(&self, paddr: u32) -> u32 {
    self.read(mem::Read::readw, paddr)
  }

  fn readb(&self, paddr: u32) -> u8 {
    self.read(mem::Read::readb, paddr)
  }
}

impl mem::Write for MemoryMap {
  fn writew(&mut self, paddr: u32, word: u32) {
    self.write(mem::Write::writew, paddr, word)
  }

  fn writeb(&mut self, paddr: u32, byte: u8) {
    self.write(mem::Write::writeb, paddr, byte)
  }
}
