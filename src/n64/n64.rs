use cart::Cart;
use cpu::{self, mem, Cpu};
use n64::MemoryMap;
use pif_rom::PifRom;
use rsp::Rsp;

pub struct N64 {
  cpu: Cpu,
  mem_map: MemoryMap,
  cart: Option<Box<Cart>>,
}

impl N64 {
  pub fn new(pif_rom: mem::Mem<PifRom>) -> Self {
    Self {
      cpu: Cpu::new(),
      mem_map: MemoryMap::new(pif_rom, Rsp::new()),
      cart: None,
    }
  }

  pub fn insert_cart(&mut self, cart: Cart) {
    self.cart = Some(Box::new(cart))
  }

  pub fn power_on(&mut self) {
    self.cpu.power_on_reset()
  }

  pub fn start(&mut self) {
    loop {
      let word = self.cpu.fetch(&self.mem_map);
      let instr = cpu::decode_instruction(word);
      self.cpu.execute(&instr, &mut self.mem_map);
    }
  }
}
