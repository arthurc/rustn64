use std::marker::PhantomData;
use std::ops::{Index, RangeFull};

use byteorder::ByteOrder;

pub struct Rom<T: ByteOrder> {
  buf: Vec<u8>,
  byte_order: PhantomData<T>,
}

impl<T: ByteOrder> Rom<T> {
  pub fn new(buf: Vec<u8>) -> Rom<T> {
    Rom {
      buf: buf,
      byte_order: PhantomData,
    }
  }

  pub fn read_word(&self, addr: u32) -> u32 {
    T::read_u32(&self.buf[addr as usize..])
  }

  pub fn read_ascii_string(&self, addr: u32, count: usize) -> String {
    String::from_utf8_lossy(&self.buf[addr as usize..addr as usize + count])
      .trim_right_matches('\0')
      .to_string()
  }

  pub fn byte_swap(&mut self) {
    for addr in 0..self.buf.len() / 2 {
      let x1 = self.buf[2 * addr];
      let x2 = self.buf[2 * addr + 1];

      self.buf[2 * addr] = x2;
      self.buf[2 * addr + 1] = x1;
    }
  }

  pub fn endian_swap(&mut self) {
    panic!("TODO")
  }
}

impl<T: ByteOrder> Index<RangeFull> for Rom<T> {
  type Output = [u8];

  fn index(&self, _index: RangeFull) -> &[u8] {
    &self.buf[..]
  }
}

impl<T: ByteOrder> Index<u32> for Rom<T> {
  type Output = u8;

  fn index(&self, index: u32) -> &u8 {
    &self.buf[index as usize]
  }
}

#[cfg(test)]
mod tests {

  use super::*;
  use byteorder::{BigEndian, LittleEndian};

  #[test]
  fn test_that_the_byte_order_type_is_honored() {
    let be = Rom::<BigEndian>::new(vec![0, 0, 0, 1]);
    assert_eq!(1, be.read_word(0));

    let le = Rom::<LittleEndian>::new(vec![1, 0, 0, 0]);
    assert_eq!(1, le.read_word(0));
  }

  #[test]
  fn test_that_a_string_can_be_read() {
    let rom = Rom::<LittleEndian>::new(vec![
      0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21,
    ]);

    assert_eq!("Hello World!", rom.read_ascii_string(0, 12));
  }

  #[test]
  fn test_that_strings_are_read_and_zeros_trimmed() {
    let rom = Rom::<LittleEndian>::new(vec![
      0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]);

    assert_eq!("Hello", rom.read_ascii_string(0, 12));
  }

  #[test]
  fn test_that_strings_can_be_read_from_the_middle_of_a_buffer() {
    let rom = Rom::<LittleEndian>::new(vec![
      0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21,
    ]);

    assert_eq!("Wor", rom.read_ascii_string(6, 3));
  }

  #[test]
  fn test_that_byte_swap_swaps_bytes() {
    let mut rom = Rom::<LittleEndian>::new(vec![0x12, 0x34, 0x56, 0x78]);

    rom.byte_swap();

    assert_eq!([0x34, 0x12, 0x78, 0x56], rom[..]);
  }
}
